def marks():
    return { "de" : [r"\[\[", r"\]\]"],#marks: except for translations/Übersetzungen
             "en" : [r"\{\{", r"\}\}"] }#sense: not actually word, but a headline

def relations():
    return {"en" : [ r"Translations"
                    ,r"Synonyms|Etymology [123]|Verb|Noun|Adjective"
                     ,r"Descendants|Diminutives"
#                     ,r"Hypernyms|Hyponyms|Hyperonyms|Related terms"
    ],
            "de" : [  r"Übersetzungen"
                     ,r"Synonyme|Herkunft|Bedeutungen"
                     ,r"Wortbildungen|Sinnverwandte Wörter|Abgeleitete Begriffe|Charakteristische Wortkombinationen|Verkleinerungsformen"
#                     ,r"Oberbegriffe|Unterbegriffe"
            ]}

def section():
    return {r"Synonyms|Etymology [123]|Verb|Noun|Adjective" : ["===", "==="],
           r"Descendants|Diminutives" : ["===", "==="],
           r"Hypernyms|Hyponyms|Hyperonyms|Related terms" : ["===", "==="],           
           r"Translations" : [r"===", "==="],
           r"Synonyme|Herkunft|Bedeutungen" : ["\{\{", "\}\}"],
           r"Übersetzungen" : ["\{\{", "\}\}"],
           r"Oberbegriffe|Unterbegriffe" : ["\{\{", "\}\}"],
           r"Wortbildungen|Sinnverwandte Wörter|Abgeleitete Begriffe|Charakteristische Wortkombinationen|Verkleinerungsformen" : ["\{\{", "\}\}"],            
    }
def relatedness():
    return {r"Translations" : 1.1,
            r"Übersetzungen" : 1.1,
            
            r"Synonyms|Etymology [123]|Verb|Noun|Adjective" : 1.3,
            r"Synonyme|Herkunft|Bedeutungen" : 1.3,
            
            r"Wortbildungen|Sinnverwandte Wörter|Abgeleitete Begriffe|Charakteristische Wortkombinationen|Verkleinerungsformen" : 1.3,            
            r"Descendants|Diminutives" : 1,
            r"Hypernyms|Hyponyms|Hyperonyms|Related terms" : 3,
            r"Oberbegriffe|Unterbegriffe" : 3}

def explain(family):
    pass


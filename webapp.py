#-*- mode: python -*-
#import aiohttp_debugtoolbar # local debug
#from aiohttp_debugtoolbar import toolbar_middleware_factory

import asyncio, aiohttp, requests, re, os, time
import concurrent.futures
from aiohttp import web
from operator import itemgetter

with open("style.css", "r") as fo:
        css = f"<style>{fo.read()}</style>"

with open("static/page.htm", "r") as fo:
    page = fo.read()

with open("static/card.htm", "r") as fo:
    card = fo.read()

import sounds
suffixes, equiv, equiv2, aspirants = sounds.suffixes(), sounds.equiv(), sounds.equiv2(), sounds.aspirants()

import scrape
marks, relations, section, relatedness  = scrape.marks(), scrape.relations(), scrape.section(), scrape.relatedness()
lx, ly = "en", "de"
fuzz = 0.1
whitespace = "_"
url = "http://{}.wiktionary.org/w/api.php?action=query&prop=revisions&rvprop=content&format=xml&titles="

async def makeunique(somelist):
    newlist = []
    entries = {}
    for e in somelist:
        try:
            asdf = entries[e]
            entries[e] += 1            
            e = f'{e}{entries[e]}'
        except KeyError:
            entries[e] = 1            
        newlist.append(e)
    return newlist


async def resolve(word, lang):
# early kickout
    kickouts = [r"reflexiv|Singular|Suffix|Präposition|Derivatem|süddeutsch|Special:ApiFeatureUsage|Image:|(Gen[ei]|Nomina)tiv|en-noun",
                r"(ux|IPA|audio|qualifier)\|",
                r"(lb|Ü|cog|m|t(-simple|\+)?)\|[^(de)(en)]" ]
    for k in kickouts:
        if re.search(k,word):
            return None
# translation    
    try: 
        ltext = lang[0]
        ltag = lang[1]
        if lang[0] is "en":
            word = re.sub(r"(German: )?\{{0,2}(cog|t\+?|t-simple)\|de\|([A-Za-zÄÖÜäöüß ]*)(\|t=[A-Za-zÄÖÜäöüß ]*)?(\|[mfn])?(\|langname=German)?(\|interwiki=1)?\}{0,2}", r"\3", word)
        if lang[0] is "de":
            word = re.sub(r"\{{0,2}(l|cog)\|en\|([A-Za-zÄÖÜäöüß ]*)\}{0,2}", r"\2", word)
# population        
    except:
        if lang == "en":
            word = re.sub(r"\{\{[ml]\|en\|", r"", word)
        elif lang == "de":
            word = re.sub(r"\{\{l\|de\|", r"", word)
    word = re.sub(r"\|(g=)?[fmn]\}{0,2}", r"", word)
    word = re.sub(r"\[\[", r"", word)
    word = re.sub(r"\|alt=.*", r"", word)
    #    word = re.sub(r"\}\}(\\?n\*)?:??;?,?(\{\{..+\}\}:)?", r"", word)    
    word = re.sub(r"\]\]?(\\n)?:?\.?", r"", word)
    word = re.sub(r"['}+/]", r"", word)
# late kickout
    kickouts = [r"[^A-Za-zäÄöÖüÜß\-,; ]"]
    for k in kickouts:
        if re.search(k,word):
            return None
    return word


async def listfetch(url, lang, querylist):
    chunksno = len(querylist) // 50    # num of complete 50-packs
    allchunks = [querylist[chno*50:(chno+1)*50] for chno in range(0,chunksno)]
    lastchunk = allchunks.append(querylist[(chunksno*50):(chunksno*50)+(len(querylist) % 50)]) #eg 100-198
    atp = [("{}|" * len(fpack)).format(*fpack) for fpack in allchunks]
    results = []
    for querystring in atp:
        async with aiohttp.ClientSession() as session:
            async with session.get(f'{url.format(lang)}{querystring}') as resp:
                results.append(await resp.text())
    
#     with concurrent.futures.ThreadPoolExecutor(max_workers=20) as executor:
#         loop = asyncio.get_event_loop()
#         futures = [
#             loop.run_in_executor(
#                 executor,
#                 requests.get,
#                 f'{url.format(lang)}{querystring}'
# #                   , params="titles=querystring" 
#             )
#             for querystring in atp
#         ]
#         results = []
#         results = [resp.text for resp in await asyncio.gather(*futures)]
    return results

async def detect(text, lang, mark, relation, myrel):
    words = re.split('(\. |\\n|\s(?![^{}=]{0,30}[}=])(?!.{0,15},))', text)    
    results = []
    w = 0
    while w < len(words):
        if re.search(section[relation][0]+relation, words[w]):
            w += 1
            cap = 0
            while w < len(words) and not re.search(section[relation][0], words[w]):
                if re.search(mark, words[w]):
                    resword = await resolve(words[w], lang)
                    if resword != None:
                        if not resword.isspace():
                            reswords = re.split('[,;]', resword)
                            try:
                                for rw in reswords:
                                    results.append(rw.strip())
                            except TypeError:
                                pass
                w += 1
#                cap += 1
        else:
            w += 1
            
    relmulti = relatedness[relation]*myrel[0]
    relhist = [relation]+myrel[1]
    newreltup = (relmulti, relhist)
    return results, newreltup

async def multigettranslations(textlist, lfrom, lto, myrel):
#    mark = f'\|{lto}\|'
    mark = "\|" + lto  + "\|"
    results = []
    for text in textlist:
        for relation in relations[lfrom][:1]:            
            translations = await detect(text, (lfrom, lto), mark, relation, myrel)
            try:
                proof = translations[0][0]
                results.append(translations)
            except IndexError:
                pass
    return results

async def multigetpopulation(textlist, lang, myrel):
    mark = marks[lang][0]        
    results = []
    for text in textlist: # per 50-pack
        articlelist = re.split('pageid', text) # todo: further constrain the regexp
        for article in articlelist:
        # debug: limit relations             vv     
            for relation in relations[lang][1:]:
                population = await detect(article, lang, mark, relation, myrel)
                try:
                    proof = population[0][0]
                    results.append(population)
                except IndexError:
                    pass
    return results

async def justify(ARG1,ARG2):
    diff = len(ARG1) - len(ARG2)
    if diff > 0:
        ARG2 = f'{ARG2}{whitespace * diff}'            
    elif diff < 0:
        ARG1 = f'{ARG1}{whitespace * (-diff)}'
    for arg in [ARG1, ARG2]:
        arg = f'{4*whitespace}{arg}{4*whitespace}'
    return ARG1, ARG2

async def smartrotate(counterword, alternative):
#    diff = len(counterword.strip(whitespace)) - len(alternative.strip(whitespace))
#    rotated = []    
    rotated = [ (alternative[l:]+alternative[:l], l) for l in range(0,len(alternative),1) ]
    return rotated

async def monogram(a0, b0):
    try:
        if a0 in equiv[b0]:
            return True
    except KeyError:
        pass

async def exactmonogram(a0, b0):
    if a0 == b0:
        return True

async def bigram(a0, a1, b0, b1):
    try:
        if str(a0+a1) in equiv2[str(b0+b1)]:
            return True
    except KeyError:
        pass

async def exactbigram(a0, a1, b0, b1):
    if a0 == b0 and a1 == b1:
        return True

async def crossbigram(a0, a1, b0, b1):
    try:        
        if a0 in equiv[b1] and a1 in equiv[b0]:
            return True
    except KeyError:
        pass
    
async def exactcrossbigram(a0, a1, b0, b1):
    if a0 == b1 and a1 == b0:
        return True

async def crossmonogram(x0, y1):
    try:
        if x0 in equiv[y1]:
            return True
    except KeyError:
        pass

async def exactcrossmonogram(x0, y1):
    if x0 == y1:
        return True


async def match(this, that, r, myrel):
    relscore = myrel[0]
    gesamtscore, gesamtscore2, score, matches, matches2, l, multi, rmulti, lmulti, history, history2, gogogo = 0, 0, 0, [], [], -1, 1, 1, 1, [], [], True
#    if abs(r) == 0:
#        multi = 20 # first letter match bonus
    while l < len(this)-1:
        l += 1
        if not gogogo:
            score *= max(rmulti,lmulti,multi)
            history.append(f'----- | {score} * rlm {max(rmulti, lmulti, multi)}')
        gesamtscore += score
        history.append(f'----- | +{score} = {gesamtscore}')        
        score = 0
        gogogo = False
        if l == len(this)-r: # word boundary
            gesamtscore2 = gesamtscore
            matches2 = matches
            history2 = history
            gesamtscore = 0
            matches = []
            history = []
            multi = 1
        a0, b0 = this[l].casefold(), that[l].casefold()
        try:
            a1, b1 = this[l+1].casefold(), that[l+1].casefold()
            if await bigram(a0, a1, b0, b1) and l+1 != len(this)-r:
                score += 3
                multi += 3
                lmulti += 0
                rmulti += 0                
                if await exactbigram(a0, a1, b0, b1):
                    score += 8
                    multi += 6
                matches.append(l)
                matches.append(l+1)
                history.append(f'{round(gesamtscore,0)} | {round(gesamtscore2,0)} | BG {round(score,1)}x{multi}')
                l += 1
                continue
        except (IndexError, KeyError):
           pass
        if await monogram(a0, b0):
            score += 1
            multi += 1
            rmulti = 1                
            if await exactmonogram(a0, b0):
                score += 2
                multi += 2
                matches.append(l)
            else:
                matches.append(l+fuzz)                
            history.append(f'{round(gesamtscore,0)} | {round(gesamtscore2,0)} | monogram score:{round(score,1)} multi: {multi}')            
            continue
        try:
            a1, b1 = this[l+1].casefold(), that[l+1].casefold()
            if await crossbigram(a0, a1, b0, b1) and l+1 != len(this)-r:
                score += 2
                multi += 2                                
                if await exactcrossbigram(a0, a1, b0, b1):
                    score += 2
                    multi += 2                               
                matches.append(l+fuzz)
                matches.append(l+1+fuzz)
                history.append(f'{round(gesamtscore,0)} | {round(gesamtscore2,0)} | crossbigram {round(score,1)}x{multi}')                            
                l += 1
                continue
            elif await crossmonogram(a0, b1) and l+1 != len(this)-r:
                lmulti = multi
                multi = 1
                score += 1
                if await exactcrossmonogram(a0, b1):
                    lmulti += 1
                    score += 2
                matches.append(l+fuzz)
                matches.append(l+1+fuzz)
                history.append(f'{round(gesamtscore,0)} | {round(gesamtscore2,0)} | Cm {round(score,1)}x{multi}')
                lmulti += 1
                rmulti = 0
                
                l += 1
                continue
            elif await crossmonogram(a1, b0) and l+1 != len(this)-r:
                rmulti = multi
                multi = 1
                score += 1
                if await exactcrossmonogram(a1, b0):
                    rmulti += 1
                    score += 2
                matches.append(l+fuzz)
                matches.append(l+1+fuzz)
                rmulti += 1
                lmulti = 0
                history.append(f'{round(gesamtscore,0)} | {round(gesamtscore2,0)} | crossmonogram score: {round(score,1)} multi: {multi}')               
                l += 1
                continue
        except (IndexError, KeyError):
            pass
        multi = 1
        gogogo = True # skip maths in case of no hit
    if gesamtscore2 > gesamtscore:
        gesamtscore = gesamtscore2
        matches = matches2
        history = history2
        history.append('->')
    if len(matches) < len(that):
        history.append(f'{round(gesamtscore,0)} | {round(gesamtscore2,0)} | {round(gesamtscore, 1)} / {1+((len(that)-len(matches)))}')
        try:
            gesamtscore /= ((len(that.rstrip(suffixes))-len(matches)))
        except:
            gesamtscore /= 20
    history.append(f'{int(round(gesamtscore,0))} / {round(relscore/5,2)}')    
    gesamtscore /= (relscore/5)
    history.append(f'{int(round(gesamtscore,0))}')
    return gesamtscore, matches, history

async def color(winner, where, offset):
   fuzzymatches = [w-fuzz for w in where if w%1 != 0]
   where = [w for w in where if w%1 == 0]
   formatiert = ["<span class='w'>"+winner[w]+"</span>" if w in where else "<span class='f'>"+winner[w]+"</span>"  if w in fuzzymatches else "<span class='n'>"+winner[w]+"</span>" for w in range(0,len(winner),1)]   
   formatiert = formatiert[-offset:] + formatiert[:-offset] #rotate=backwards,thus -
   winner = "".join(formatiert)
   return winner

async def mnemo_translate(queries, lx, ly, myrel):
    queries += [f'{q}/translations' for q in queries]
    texts = await listfetch(url, lx, queries)
    if texts:
        translations = await multigettranslations(texts, lx, ly, myrel)
    return translations, texts

async def mnemo_populate(words, lang, myrel):
    texts = await listfetch(url, lang, words)
    if texts:    
        population = await multigetpopulation(texts, lang, myrel)
    return population, texts

async def flatten(somelist):
    newlist = [item for sublist in somelist for item in sublist]
    return newlist

async def smartcount(groupstruct):
    smartlen = 0
    for sometuple in groupstruct:
        try:
            smartlen += len(sometuple[0])
        except IndexError:
            pass
    return smartlen


async def makestars(score, length, maxstars):
    count = int(score/10)
    if count > maxstars:
        count = maxstars
    goodstar = "&bigstar;"#"&#9648;"
    nogoodstar = "&star;"#"&#9649;"
    stars = f'{count*goodstar}{(maxstars-count)*nogoodstar}'# ({round(score, 1)})'
    return stars

async def mnemo(query, lx, ly):
    translations, tltexts = await mnemo_translate([query], lx, ly, (1, []))
    if translations:
        trpopulation, trpopulationtexts = await mnemo_populate(translations[0][0], ly, translations[0][1])
    else:
        trpopulation, trpopulationtexts = [], []
    ownpopulation, ownpopulationtexts = await mnemo_populate([query], lx, (1, []))
    hiddentranslations, hiddentranslationstexts = [], []
    for rels in ownpopulation:
        htrraw = await mnemo_translate(rels[0], lx, ly, rels[1])
        hiddentranslations.append(htrraw[0])
        hiddentranslationstexts.append(htrraw[1])

    hiddentranslations = await flatten(hiddentranslations)
    hiddentranslationstexts = await flatten(hiddentranslationstexts)
    
    htrpopulation, htrpopulationtexts = [], []
    for rels in hiddentranslations:
        htrpraw = await mnemo_populate(rels[0], ly, rels[1])
        htrpopulation.append(htrpraw[0])
        htrpopulationtexts.append(htrpraw[1])        
    htrpopulation = await flatten(htrpopulation)
    htrpopulationtexts = await flatten(htrpopulationtexts)    

    trpop2population, trpop2populationtexts = [], []
    htrpop2population, htrpop2populationtexts = [], []        
    if (await smartcount(translations)+await smartcount(trpopulation)+await smartcount(hiddentranslations)+await smartcount(htrpopulation) < 20) and trpopulation:
        trpopulation.sort(key=lambda t: t[1])
        searchtrpopulation = [t for t in trpopulation[:2] if t[1][0]<2]
        for rels in trpopulation:
            trp2raw = await mnemo_populate(rels[0], ly, rels[1])
            trpop2population.append(trp2raw[0])
            trpop2populationtexts.append(trp2raw[1])
        trpop2population = await flatten(trpop2population)
        trpop2populationtexts = await flatten(trpop2populationtexts)
        if (await smartcount(translations)+await smartcount(trpopulation)+await smartcount(hiddentranslations)+await smartcount(htrpopulation) < 20) and htrpopulation:
            htrpopulation.sort(key=lambda t: t[1][0])
            htrpopulation = [t for t in htrpopulation[:2] if t[1][0]<2]
            for rels in htrpopulation:
                htrp2raw = await mnemo_populate(rels[0], ly, rels[1])
                htrpop2population.append(htrp2raw[0])
                htrpop2populationtexts.append(htrp2raw[1])
            htrpop2population = await flatten(htrpop2population)
            htrpop2populationtexts = await flatten(htrpop2populationtexts)

    rellists = [
                translations
                ,ownpopulation
                ,trpopulation
                ,trpop2population
                ,hiddentranslations
                ,htrpopulation
                ,htrpop2population
    ]
    allpop = []
    for rellist in rellists:
        for group in rellist:
            try:
                for item in group[0]:
                    allpop.append((item, group[1]))
            except IndexError:
                pass

    candidates = [h for h in allpop]

# justify
    justified = []
    for candidate in candidates:
        justified.append((await justify(query, candidate[0]), candidate[1]))
# drag
    dragged = []
    dragged = [(jus[0][0], await smartrotate(query, jus[0][1]), jus[1]) for jus in justified]
# judge
    judged = []
    for dra in dragged:
        for rotation in dra[1]:
            score, matches, history = await match(dra[0], rotation[0], rotation[1], dra[2])
            judged.append((dra[0], rotation[0], matches, score, rotation[1], dra[2], history))
    uniquejudged = []
    uniquejudgedctl = []    
    if judged:
        judged.sort(key=itemgetter(3), reverse=True)
        n = 0
        while len(uniquejudged) < 11:
            try:
                if judged[n][1] in uniquejudgedctl:
                    n += 1
                else:
                    uniquejudged.append(judged[n])
                    uniquejudgedctl.append(judged[n][1])
            except:
                break
#    texts = [ownpopulationtexts]
    texts = tltexts, hiddentranslationstexts, trpopulationtexts, trpop2populationtexts, htrpopulationtexts, htrpop2populationtexts
    dbg = judged#candidates#texts#translations#texts#candidates
    return uniquejudged[:10], translations, dbg

async def makecard(query, judged, translations, dbg):
    if not judged:
        cards = ""
        for n in range(0,5):
            cards += card.format(n, n, "/post", n, n, "", "", "¯\_(¨)_/¯", "<div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>","")
    else:
        choice = 0
        cards = ""
        for choice in [1,2,3,4,0]:
            # retrieve
            fquery = judged[choice][0]
            expl = judged[choice][5][1]
            fcue = re.sub(whitespace, r"", await color(judged[choice][1], judged[choice][2], judged[choice][4]))
            barecue = judged[choice][1]
            barecue = re.sub(whitespace, r"", barecue)
            fquery = await color(fquery, judged[choice][2], 0)
            fquery = re.sub(whitespace, r"", fquery)            
            scores = [j[3] for j in judged]            
            translation = ""
            try:
                translation = translations[0][0][0]
            except IndexError:
                pass
### pad
            offset = judged[choice][4]
            reverseoffset = len(query)-offset
            if reverseoffset < 1:
                reverseoffset = len(query)-offset            
            cwidth, qwidth, twidth = len(barecue), len(query), len(translation)
            swap = False
            offboard, onboard = 0, 0
            for treffer in ([round(j,0) for j in judged[choice][2]]):
                if treffer+offset >= len(barecue):
                    offboard += 1
                else:
                    onboard += 1
            if offboard > onboard:
                swap = True
            if swap == True:
                if True: #reverseoffset < 0:
                    fcue = "&nbsp;"*(len(barecue)-offset)+fcue
                    cwidth += (len(barecue)-offset)                    
                else:
                    fcue = "&nbsp;"*reverseoffset+fcue
                    cwidth += reverseoffset
            else:
                fquery = "&nbsp;"*offset+fquery
                qwidth += offset
            # pad right handside
            maxwidth = max(cwidth, qwidth, twidth)
            fpad = maxwidth-qwidth
            cpad = maxwidth-cwidth
            # make box larger with very short words
            boxdiff = 6 - max(fpad, cpad, len(query), len(barecue))
            if boxdiff > 0:
                fpad += boxdiff
                cpad += boxdiff
            #build html
            fquery = fquery+"&nbsp;"*(fpad)
            fcue = fcue+"&nbsp;"*(cpad)
#            fcue += f'<br>{offset}<br>{reverseoffset}'
### pad            
### pad (sicherung)
            # offset = judged[choice][4]
            # reverseoffset = max(len(barecue),len(query))-offset

            # cwidth, qwidth, twidth = len(barecue), len(query), len(translation)
            # if min([round(j,0) for j in judged[choice][2]]) < offset:
            #     fcue = "&nbsp;"*reverseoffset+fcue
            #     cwidth += reverseoffset
            # else:
            #     fquery = "&nbsp;"*offset+fquery
            #     qwidth += offset
            # maxwidth = max(cwidth, qwidth, twidth)
            # fpad = maxwidth-qwidth
            # cpad = maxwidth-cwidth
            # # make box larger with very short words
            # boxdiff = 6 - max(fpad, cpad, len(query), len(barecue))
            # if boxdiff > 0:
            #     fpad += boxdiff
            #     cpad += boxdiff
            # #build html
            # fquery = fquery+"&nbsp;"*(fpad)
            # fcue = fcue+"&nbsp;"*(cpad)
### pad            
            fcue = f'<div onclick="" class="tooltip">{fcue}</div><span class="tooltiptext">{fcue} ist {scrape.explain(expl)} von {fquery.strip()}</span>'
#            fcue += f'<br>{swap}<br>{offset}/{reverseoffset}'
            # append explanatoin
            fcue += f'<span class="explanation">{re.sub(r"[^A-Za-zÖöÄäÜüß-].*",r"",expl[0])}</span>'
            # add stars
            fcue += f'<span class="stars">{await makestars(scores[choice],len(barecue),5)}</span><span class="points">{int(round(scores[choice],0))}</span>'
            # append translation
            fcue += f'<span class="translation">{translation}</span>'

            
            
            nc = choice+1 #next and prev choice
            pc = choice-1
            if nc >= 5:
                nc = 0
            if pc <= -1:
                pc = 4
            
            dot = '<span class="dotsym"><a href="{}">●</a></span>'

            cue0 = re.sub(whitespace, r"", judged[0][1])
            cue0 = cue0[-judged[0][4]:] + cue0[:-judged[0][4]]
            cue1 = re.sub(whitespace, r"", judged[1][1])
            cue1 = cue1[-judged[1][4]:] + cue1[:-judged[1][4]]
            cue2 = re.sub(whitespace, r"", judged[2][1])
            cue2 = cue2[-judged[2][4]:] + cue2[:-judged[2][4]]
            cue3 = re.sub(whitespace, r"", judged[3][1])
            cue3 = cue3[-judged[3][4]:] + cue3[:-judged[3][4]]
            cue4 = re.sub(whitespace, r"", judged[4][1])
            cue4 = cue4[-judged[4][4]:] + cue4[:-judged[4][4]]

            cue0, cue1, cue2, cue3, cue4 = await makeunique([cue0, cue1, cue2, cue3, cue4])
            cueX = [cue0, cue1, cue2, cue3, cue4][choice]
            cuePC = [cue0, cue1, cue2, cue3, cue4][pc]
            cueNC = [cue0, cue1, cue2, cue3, cue4][nc]
            cue0, cue1, cue2, cue3, cue4, cueX, cueNC, cuePC = [re.sub(" ", "-", j) for j in [cue0, cue1, cue2, cue3, cue4, cueX, cueNC, cuePC]] # get rid of space in URL
            
            navs = [dot.format(f'#{cue0}'), dot.format(f'#{cue1}'), dot.format(f'#{cue2}'), dot.format(f'#{cue3}'), dot.format(f'#{cue4}')]
            navs[choice] = f'<span class="nav dotsym"><a href="#{cueX}">●</a></span>'
            navbar = "".join(navs)
            navigation = f'<br><span class="next"><a href="#{cuePC}">❮</a>{navbar}<a href="#{cueNC}">❯</a></span>'
# ---------- #debug with: dbg=lytexts
#             diagnose = []
#             for texts in dbg:
#                 for text in texts:
#                     twords = re.split('(\. |\\n|\s(?![^{}=]{0,30}[}=])(?!.{0,15},))', text)
#                     treswords = [f'<span style="color:grey;">{t} >> </span><span style="color:white;">{await resolve(t, ("en", "de"))}</span>' if (await resolve(t, ("en", "de")) !=None) else f'<span style="color:red;">{t} -> </span><span style="color:red;">""{await resolve(t, ("en", "de"))}""</span>' for t in twords]
#                     for t in treswords:
#                         diagnose.append(t)
#             fcue += '<span class="dbg">'+"<br>".join(diagnose)+'</span>'
# # ---------- #debug with: dbg=candidates
#            fcue += '<span class="dbg"><ol>' + "".join(["<li>"+str(d[1])+"<br>- "+"<br>- ".join([str(db) for db in d[6]])+"<br><br></li>" for d in dbg]) + '</ol></span>'
# ----------
        
            onecard = card.format(cueX, cueX, "/post", choice, choice, "", "", fquery, "<div>"+fcue+"</div>", navigation)
            cards += onecard
    return cards

async def makestartpage():
    fquery = "soundslike"
    fcue = "&nbsp;"*len(fquery)
    fcue += f'<br><span class="stars">{await makestars(500,5, 5)}</span><span class="points">{int(round(500,0))}</span>'
    # append translation
    fcue += f'<br><span class="translation"></span>'
    navbar = ""
    cards = card.format("Welcome", "Welcome", "/post", "W", "W", "", "", fquery, "<div>"+fcue+"</div>", "")
    return cards        



async def handler(request):
    lx, ly = "en", "de"
    query = format(request.match_info.get('query', "soundslike")) # get
    try:
        judged, translations, dbg = await mnemo(query, lx, ly)
    except:
        judged, translations, dbg = await mnemo(query, lx, ly)        
    fpage = page.format(query, await makecard(query, judged, translations, dbg))
    resp = web.Response(text=fpage+css, content_type='text/html')
    return resp

async def noqhandler(request):
    fpage = page.format("soundslike", await makestartpage())
    return web.Response(text=fpage+css, content_type='text/html')

async def posthandler(request):
    postdata = await request.post()
    query = postdata['query'].strip()
#    query = query.strip() #remove whitespace from left and right word-sides
    url = request.app.router['query'].url_for(query=query)
    raise web.HTTPFound(location=url)

app = web.Application()
#aiohttp_debugtoolbar.setup(app) # comment out for production deployment
app.add_routes([web.route('*', '/', noqhandler),
                web.get('/{query:[A-Za-z]*}', handler),
#                web.post('/{query:[A-Za-z]*}', handler),                
                web.post('/post', posthandler)])
app.router.add_resource(r'/{query:[A-Za-z]}', name='query')
web.run_app(app, port=os.getenv('PORT'))

def suffixes():
    v = ["en"]
    n = ["ion", "ment"]
    return "("+"|".join(["|".join(v),"|".join(n)])+")"

def aspirants():
    return ["h"]

def equiv2():
    return {"ck" : ["ck", "ch"],
            "ie" : ["ie", "ee", "ei"],
            "eu" : ["ui", "eu"],
            "ch" : ["gh", "eu"],            
            "(p[a-z])|([a-z]p)" : ["ff"]}

def equiv():
    return {
        # german to english (for use in english to german method) (repair match() later)
        # whitespace        
        "_" : [""], 
        # vowels / sonorants with no place of obstruction
        "a" : ["a", "o"],
        "u" : ["a", "u", "o"],            
        "o" : ["o", "u", "e"],
        "ä" : ["e", "a", "o"],            
        "e" : ["i", "e", "a", "o"],
        "ö" : ["e", "o"],
        "ü" : ["a", "e", "i", "u", "o"],            
        "i" : ["i", "e", "y"],
        "y" : ["y", "i"],
        
        # bilabial 
        "b" : ["b", "p", "v", "f", "w"],
        "p" : ["b", "p"],            
        "m" : ["m"],

        # labiodental
        "f" : ["f", "p"],
        "v" : ["v", "w", "f"],
        "w" : ["v", "w"],            

        # alveolar
        "d" : ["d", "t"],
        "t" : ["t", "d"],            
        "n" : ["n"],
        "s" : ["s", "z", "t"],
        "z" : ["z", "t", "c", "s"],
        "l" : ["l"],
        "r" : ["r"],            

        # palatal        
        "j" : ["j", "y"],            

        # velar and beyond
        "g" : ["g", "c"],
        "c" : ["c", "k", "g"],
        "k" : ["k", "c"],
        "q" : ["q"],            
        "h" : ["h"],
        
        "x" : ["x"] }
